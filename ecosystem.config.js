module.exports = {
    apps : [
        {
          name: "nuxt",
          script: "./node_modules/nuxt/bin/nuxt-start",
          env: {
              "HOST": "127.0.0.1",
              "PORT": 3000,
              "NODE_ENV": "production",
          },
          watch:true,
        }
    ]
  }